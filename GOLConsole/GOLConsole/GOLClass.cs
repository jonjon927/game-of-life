﻿/* John VanSickle 4/5/2013 CIS 244 Game of Life Project
 * 
 * This class is designed for be able to be instantiated by any console and methods used to manipulate the object.
 * GOL is a simple game that is fun to just watch, but can be attributed to some realistic properties such as
 * population growth and decline.
 * 
 * Algorithm/analysis:
 *      -Creates a grid of structs that tracks currently alive, future alive, and age
 *      -methods manipulate the grid as called by the console that instantiated the GOL object
 *      -The overall grid has an outer frame of zeros for the checking sequence - used grid is 40X60, actual grid is 42X62
 *      -Print() is designed for use with the 42X62 grid and would need adjusted for bigger or smaller grids
 *      -DoGeneratoins() does a single generation!! Need to loop as part of the User Interface (console/windows)
 *      -GetSTats() and PrintStats are for collecting stats on status of each cell and keeping track of each
 *          record to be reported after a set of generations is complete.
 *      
 * Additional Comments:
 *      -Designed in many methods so that it is more easily organized
 *      -There is a tiny bit of flicker as in the key typist
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOLConsole
{
    class GOLClass
    {
        //class levels because they are used by two methods
        int numAlive;
        int numDead;
        int numAge0;
        int numAge1;
        int numAge2;
        int numAge3;
        int numAge4Plus;

        Random rnd = new Random();

        //for each item in 2D array. Will contain 3 parts
        struct cell
        {
            public int cAlive;  //currently alive
            public int fAlive;  //future alive for print
            public int age; //used for color reference during print
        }

        private cell[,] grid = new cell[42,62];

        //fills with random 0 or 1 to determine whether initially alive
        public void FillGridRandom()
        {
            for (int i = 1; i < 41; i++)
                for (int j = 1; j < 61; j++)
                {
                    grid[i, j].cAlive = rnd.Next(0, 2);

                    //if randomly slected alive, sets age to zero
                    if (grid[i, j].cAlive == 1)
                        grid[i, j].age = 0;
                }
        }

        //places three known patterns randomly in array
        public void FillGridKnown()
        {
            int num = rnd.Next(3, 37);
            int num2 = rnd.Next(3, 57);

            //makes a R-Pentomino somewhere on grid
            grid[num, num2].cAlive = 1;
            grid[num + 1, num2].cAlive = 1;
            grid[num - 1, num2].cAlive = 1;
            grid[num - 1, num2 + 1].cAlive = 1;
            grid[num, num2 - 1].cAlive = 1;

            num = rnd.Next(3, 37);
            num2 = rnd.Next(3, 57);

            //makes a glider randomly placed
            grid[num, num2].cAlive = 1;
            grid[num, num2 + 1].cAlive = 1;
            grid[num - 1, num2 + 1].cAlive = 1;
            grid[num - 1, num2 - 1].cAlive = 1;
            grid[num - 2, num2 + 1].cAlive = 1;

            num = rnd.Next(3, 37);
            num2 = rnd.Next(3, 57);

            //makes an acorn randomly placed
            grid[num, num2].cAlive = 1;
            grid[num, num2 + 1].cAlive = 1;
            grid[num - 2, num2 + 1].cAlive = 1;
            grid[num - 1, num2 + 3].cAlive = 1;
            grid[num, num2 + 4].cAlive = 1;
            grid[num, num2 + 5].cAlive = 1;
            grid[num, num2 + 6].cAlive = 1;
        }

        //used to print the alive cells
        public void Print()
        {
            for (int i = 1; i < 41; i++)
                for (int j = 1; j < 61; j++)
                {
                    if (grid[i, j].cAlive == 1)
                    {
                        //switch statement instead of a lot of if's to see what color it is to print
                        switch (grid[i, j].age)
                        {
                            case 0:
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write("*");
                                break;

                            case 1:
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write("*");
                                break;

                            case 2:
                                Console.ForegroundColor = ConsoleColor.Blue;
                                Console.Write("*");
                                break;

                            case 3:
                                Console.ForegroundColor = ConsoleColor.Magenta;
                                Console.Write("*");
                                break;

                            default:
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("*");
                                break;
                        }

                        //changes print back to white for menu reasons
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    
                    //else is if a cell is "dead"
                    else
                        Console.Write(" ");
                    
                    //tests cursor position so that the grid print stays within desired boundaries
                    if (j == 60)
                        Console.SetCursorPosition(2,i + 1);
                }
        }

        //used for a single generation, loops in program class, not GOL class
        public void DoGenerations()
        {
            for (int i = 1; i < 41; i++)
                for (int j = 1; j < 61; j++)
                {
                    //checks conditions for a dead cell
                    if (grid[i, j].cAlive == 0)
                    {
                        int neighbors = 0;

                        //nested for loop checks only cells deriectly around current cell
                        for (int w = i - 1; w <= i + 1; w++)
                            for (int q = j - 1; q <= j + 1; q++)
                            {
                                //this if checks the current cellbeing pointed to around the "dead" cell
                                if (grid[w, q].cAlive == 1)
                                    neighbors++;
                            }

                        //after calculating neighbors, rules say that if there are three then the dead cell becomes alive.
                        if (neighbors == 3)
                        {
                            grid[i, j].fAlive = 1;
                            grid[i, j].age = 0; //NEED to reset age.
                        }
                    }

                    //else implies the field is alive ie. cAlive == 1
                    else
                    {
                        //need neighbors to be at -1 because the current alive cell will add a neighbor count, which should not count
                        int neighbors = -1;

                        for (int w = i - 1; w <= i + 1; w++)
                            for (int q = j - 1; q <= j + 1; q++)
                            {
                                if (grid[w, q].cAlive == 1)
                                    neighbors++;
                            }

                        //rules say that with 0,1, or 4+ neighbors the cell dies
                        if (neighbors == 0 || neighbors == 1 || neighbors >= 4)
                        {
                            grid[i, j].fAlive = 0;
                            grid[i, j].age = 0; //set age to 0 for counting purpose
                        }

                        else  //implies that neighbor count was 2 or 3 - ie. the cell survives
                        {
                            grid[i, j].fAlive = 1;
                            grid[i, j].age++;   //age increases each survival
                        }
                    }
                }

            //update current alive status from future to be printed
            for (int i = 1; i < 41; i++)
                for (int j = 1; j < 61; j++)
                {
                    grid[i, j].cAlive = grid[i, j].fAlive;
                }
        }

        //gets stats for amounts of each age as well as how many currently alive and dead
        public void GetStats()
        {
            //vars are at class level.
            numAlive = 0;
            numDead = 0;
            numAge0 = 0;
            numAge1 = 0;
            numAge2 = 0;
            numAge3 = 0;
            numAge4Plus = 0;

            for (int i = 1; i < 41; i++)
                for (int j = 1; j < 61; j++)
                {
                    //if/else to track number dead and alive
                    if (grid[i, j].cAlive == 1)
                        numAlive++;
                    else
                        numDead++;

                    //switch counts numbers of each age instead of many if-elseif
                    switch (grid[i, j].age)
                    {
                        case 0:
                            numAge0++;
                            break;

                        case 1:
                            numAge1++;
                            break;

                        case 2:
                            numAge2++;
                            break;

                        case 3:
                            numAge3++;
                            break;

                        default:
                            numAge4Plus++;
                            break;

                    }

                    //need this to keep the dead cells at age 0 from counting
                    numAge0 = numAlive - numAge1 - numAge2 - numAge3 - numAge4Plus;
                }
        }

        //prints the collected stats in the right margin of window
        public void PrintStats()
        {
            //this is the upper right portion of the console
            Console.SetCursorPosition(65, 0);

            Console.Write("Numbers of...");

            Console.SetCursorPosition(65, 1);
            Console.Write("Alive: " + numAlive);

            Console.SetCursorPosition(65, 2);
            Console.Write("Dead: " + numDead);

            Console.SetCursorPosition(65, 3);
            Console.Write("Age0: " + numAge0);

            Console.SetCursorPosition(65, 4);
            Console.Write("Age1: " + numAge1);

            Console.SetCursorPosition(65, 5);
            Console.Write("Age2: " + numAge2);

            Console.SetCursorPosition(65, 6);
            Console.Write("Age3: " + numAge3);

            Console.SetCursorPosition(65, 7);
            Console.Write("Age4+: " + numAge4Plus);
        }
    }
}

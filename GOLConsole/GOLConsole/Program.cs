﻿/* John VanSickle 4/5/2013 CIS 244 Game of Life Project
 * 
 * This program class is designed to create a GOL object and use its methods to display a working GOL in
 * a console environment.
 * 
 * Algorithm:
 *      -Provide user with two options of grids
 *      -display option
 *      -Ask user for number of generations
 *      -Use GOL class method to generate and print each change in each generation
 *      -Allow user to restart from new grid, continue current, or close
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOLConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //needs to be class level to be used in do-while loop
            int numOfGenerations = 0;
            string choice;

            try
            {
                do
                {

                    //sets up console for proper displaying
                    Console.SetWindowSize(84, 52);
                    Console.SetBufferSize(84, 52);
                    Console.Title = "John VanSickle: GOL";

                    //creates new instance of GOF class
                    GOLClass thing = new GOLClass();

                    //pre-write stuff for header of console
                    int generation = 0;
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine("Generation: " + generation.ToString("000"));

                    bool escape = false;

                    do
                    {
                        //lower menu option set up
                        Console.SetCursorPosition(0, 45);
                        Console.WriteLine("1: Random Grid   2: Three known patterns");

                        //result and out do while loop keeps the menu option up if improper input is entered
                        string result = Console.ReadLine();

                        if (result == "1")
                        {
                            //allows exit of do-while loop
                            escape = true;

                            //class method
                            thing.FillGridRandom();
                            //sets cursor so print works properly
                            Console.SetCursorPosition(2, 1);
                            //class method
                            thing.Print();
                        }
                        else if (result == "2")
                        {
                            //allows exit of do-while loop
                            escape = true;
                            //class method
                            thing.FillGridKnown();
                            //sets cursor for proper printing
                            Console.SetCursorPosition(2, 1);
                            //class method
                            thing.Print();
                        }

                    } while (escape != true);

                    //do-while checks use input for validity
                    do
                    {
                        //will write over user input for next input
                        Console.SetCursorPosition(0, 46);
                        Console.Write("Please enter number of generations (0 - 500): ");
                        numOfGenerations = int.Parse(Console.ReadLine());

                    } while (numOfGenerations <= 0 || numOfGenerations > 500);

                    //count for how many times to call DoGenerations
                    int count = 0;

                    do
                    {
                        //class method
                        thing.DoGenerations();
                        //for proper printing
                        Console.SetCursorPosition(2, 1);
                        //class method
                        thing.Print();
                        //for proper printing
                        Console.SetCursorPosition(0, 0);
                        generation++;
                        Console.Write("Generation: " + generation.ToString("000"));

                        count++;
                    } while (count < numOfGenerations);

                    //class method
                    thing.GetStats();
                    //class method
                    thing.PrintStats();

                    #region             //-------this region is for the repeating or new choice or quit - kind of ugly

                    Console.SetCursorPosition(0, 47);
                    Console.Write("Select 1 to restart, 2 to continue, 3 quit: ");
                    
                    choice = Console.ReadLine();

                    if (choice == "1")
                    {
                        Console.Clear();
                    }
                    else if (choice == "2")
                    {
                        //do-while checks use input for validity
                        do
                        {
                            //will write over user input for next input
                            Console.SetCursorPosition(0, 46);
                            Console.Write("                                                  ");
                            Console.SetCursorPosition(0, 46);
                            Console.Write("Please enter number of generations (0 - 500): ");
                            numOfGenerations = int.Parse(Console.ReadLine());

                        } while (numOfGenerations <= 0 || numOfGenerations > 500);

                        //count for how many times to call DoGenerations
                        count = 0;

                        do
                        {
                            //class method
                            thing.DoGenerations();
                            //for proper printing
                            Console.SetCursorPosition(2, 1);
                            //class method
                            thing.Print();
                            //for proper printing
                            Console.SetCursorPosition(0, 0);
                            generation++;
                            Console.Write("Generation: " + generation.ToString("000"));

                            count++;
                        } while (count < numOfGenerations);

                        //class method
                        thing.GetStats();
                        //class method
                        thing.PrintStats();

                        Console.SetCursorPosition(0, 48);
                        Console.Write("Thanks for playing the GAME OF LIFE!\nHit any key to exit..");

                        Console.ReadKey();
                        choice = "3";
                    }

                } while (choice != "3");
            }
#endregion


            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
